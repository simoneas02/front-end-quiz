import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { motion } from 'framer-motion'

import db from '../db.json'

import Widget from '../src/components/Widget'
import GitHubCorner from '../src/components/GitHubCorner'
import Footer from '../src/components/Footer'
import QuizBackground from '../src/components/QuizBackground'
import QuizContainer from '../src/components/QuizContainer'
import Input from '../src/components/Input'
import Button from '../src/components/Button'
import Link from '../src/components/Link'

export default function Home() {
  const router = useRouter()
  const [name, setName] = useState('')

  const onSubmitQuiz = event => {
    event.preventDefault()

    router.push(`/quiz?name=${name}`)
  }

  const onInputQuiz = event => setName(event.target.value)

  return (
    <QuizBackground backgroundImage={db.bg}>
      <QuizContainer>
        <Widget
          as={motion.section}
          transition={{ delay: 0, duration: 0.5 }}
          variants={{
            show: { opacity: 1, y: '0' },
            hidden: { opacity: 0, y: '100%' },
          }}
          initial="hidden"
          animate="show"
        >
          <Widget.Header>
            <h1>Title</h1>
          </Widget.Header>
          <Widget.Content>
            <p>Description</p>
            <form onSubmit={onSubmitQuiz}>
              <Input
                name="inputNameQuiz"
                value={name}
                type="text"
                placeholder="Como tu se chama bença?"
                onChange={onInputQuiz}
              />
              <Button
                type="submit"
                disabled={name.length === 0}
              >{`Bora Dale  ${name}`}</Button>
            </form>
          </Widget.Content>
        </Widget>
        <Widget
          as={motion.section}
          transition={{ delay: 0.3, duration: 0.5 }}
          variants={{ show: { opacity: 1 }, hidden: { opacity: 0 } }}
          initial="hidden"
          animate="show"
        >
          <Widget.Header>
            <h1>Frontend Quiz</h1>
          </Widget.Header>
          <Widget.Content>
            <ul>
              {db.external.map(({ projectName, userName, quiz }, index) => (
                <li key={`quiz=${index}`}>
                  <Widget.Topic
                    as={Link}
                    href={`/quiz/${projectName}___${userName}`}
                  >
                    {quiz}
                  </Widget.Topic>
                </li>
              ))}
            </ul>
          </Widget.Content>
        </Widget>
        <Footer
          as={motion.section}
          transition={{ delay: 0.5, duration: 0.5 }}
          variants={{ show: { opacity: 1 }, hidden: { opacity: 0 } }}
          initial="hidden"
          animate="show"
        />
      </QuizContainer>
      <GitHubCorner projectUrl="https://github.com/simoneas02" />
    </QuizBackground>
  )
}
