import React from 'react'
import { ThemeProvider } from 'styled-components'

import QuizPage from './'

const CoolQuiz = ({ data }) => (
  <ThemeProvider theme={data.theme}>
    <QuizPage data={data} />
  </ThemeProvider>
)

export default CoolQuiz

export const getServerSideProps = async context => {
  const [projectName, name] = context.query.id.split('___')

  const data = await fetch(`https://${projectName}.${name}.vercel.app/api/db`)
    .then(response => {
      if (response.ok) {
        return response.json()
      }

      throw new Error("Can't load data")
    })
    .then(responseObject => responseObject)
    .catch(error => console.log(`Error: ${error}`))

  return {
    props: {
      data,
    },
  }
}
