import React, { useEffect, useState } from 'react'

import db from '../../db.json'

import LoadingWidget from '../../src/components/LoadingWidget'
import QuestionWidget from '../../src/components/QuestionWidget'
import ResultWidget from '../../src/components/ResultWidget'
import QuizBackground from '../../src/components/QuizBackground'
import QuizContainer from '../../src/components/QuizContainer'

const screenStates = { QUIZ: 'QUIZ', LOADING: 'LOADING', RESULT: 'RESULT' }

export const QuizPage = ({ data }) => {
  const [screenState, setScreenState] = useState(screenStates.LOADING)
  const [results, setResults] = useState([])
  const [currentQuestion, setCurrentQuestion] = useState(0)
  const [selectedAlternative, setSelectedAlternative] = useState(undefined)
  const [isQuestionSubmited, setIsQuestionSubmited] = useState(false)

  const questionIndex = currentQuestion
  const content = data ? data : db
  const totalQuestions = content.questions.length
  const question = content.questions[questionIndex]
  const isCorrect =
    selectedAlternative === content.questions[questionIndex].answer
  const hasSelectedAlternative = selectedAlternative !== undefined

  useEffect(() => {
    setTimeout(() => {
      setScreenState(screenStates.QUIZ)
    }, 1 * 1000)
  }, [])

  const handleSubmitQuiz = event => {
    event.preventDefault()
    setIsQuestionSubmited(true)

    setTimeout(() => {
      const nextQuestion = questionIndex + 1
      const isNextQuestion = nextQuestion < totalQuestions
      addResult(isCorrect)
      if (isNextQuestion) return setCurrentQuestion(questionIndex + 1)

      setScreenState(screenStates.RESULT)
    }, 3 * 1000)

    setIsQuestionSubmited(false)
    setSelectedAlternative(undefined)
  }

  const handleInputAlternative = alternative =>
    setSelectedAlternative(alternative)

  const addResult = result => setResults([...results, result])

  return (
    <QuizBackground backgroundImage={content.bg}>
      <QuizContainer>
        {screenState === screenStates.QUIZ && (
          <QuestionWidget
            question={question}
            totalQuestions={totalQuestions}
            questionIndex={questionIndex}
            onSubmit={handleSubmitQuiz}
            onChange={handleInputAlternative}
            hasSelectedAlternative={hasSelectedAlternative}
            isCorrect={isCorrect}
            isQuestionSubmited={isQuestionSubmited}
            selectedAlternative={selectedAlternative}
          />
        )}

        {isQuestionSubmited && isCorrect && <p>Tu acertou miseraaaaa!!!</p>}
        {isQuestionSubmited && !isCorrect && (
          <p>Tu foi mal vey, tenta outra ahee!!!</p>
        )}

        {screenState === screenStates.LOADING && <LoadingWidget />}
        {screenState === screenStates.RESULT && (
          <ResultWidget results={results} />
        )}
      </QuizContainer>
    </QuizBackground>
  )
}

export default QuizPage
