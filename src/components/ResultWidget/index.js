import React from 'react'

import Widget from '../../components/Widget'
import BackLinkArrow from '../../components/BackLinkArrow'

const ResultWidget = ({ results }) => {
  const totalResults = results.reduce((total, actual) => {
    const isCorrectQuestion = actual === true

    if (isCorrectQuestion) return total + 1

    return total
  }, 0)

  return (
    <Widget>
      <Widget.Header>
        <BackLinkArrow href="/" />
        <h3>Tu acabou moh vey!!</h3>
      </Widget.Header>

      <Widget.Content>
        <p>Acertasse {totalResults} perguntas misera</p>
        <ul>
          {results.map((result, index) => (
            <li key={`${result}-${index}`}>
              {`#${index + 1} resultado: `}
              {result === true ? ' Aheee misera' : ' Só numa próxima moh vey!'}
            </li>
          ))}
        </ul>
      </Widget.Content>
    </Widget>
  )
}

export default ResultWidget
