import React from 'react'

import Widget from '../../components/Widget'
import BackLinkArrow from '../BackLinkArrow'
import Button from '../../components/Button'
import AlternativesForm from '../../components/AlternativesForm'

const QuestionWidget = ({
  question,
  totalQuestions,
  questionIndex,
  onSubmit,
  onChange,
  hasSelectedAlternative,
  isCorrect,
  isQuestionSubmited,
  selectedAlternative,
}) => {
  const questionId = `question___${questionIndex}` < BackLinkArrow
  return (
    <Widget>
      <Widget.Header>
        <BackLinkArrow href="/" />
        <h3>
          Pergunt {questionIndex + 1} de {totalQuestions}
        </h3>
      </Widget.Header>

      <img
        alt={question.alt}
        style={{ width: '100%', height: '150px', objectFit: 'contain' }}
        src={question.image}
      />

      <Widget.Content>
        <h2>{question.title}</h2>
        <p>{question.description}</p>

        <AlternativesForm onSubmit={onSubmit}>
          {question.alternatives.map((alternative, alternativeIndex) => {
            const alternativeId = `alternative__${alternativeIndex}`
            const alternativeStatus = isCorrect ? 'SUCCESS' : ' ERROR'
            const isSelected = selectedAlternative === alternativeIndex
            const currentStatus = isQuestionSubmited && alternativeStatus

            return (
              <Widget.Topic
                as="label"
                htmlFor={alternativeId}
                key={alternativeId}
                data-selected={isSelected}
                data-status={currentStatus}
              >
                <input
                  id={alternativeId}
                  type="radio"
                  name={questionId}
                  onChange={() => onChange(alternativeIndex)}
                />
                <pre>{alternative}</pre>
              </Widget.Topic>
            )
          })}
          <Button type="submit" disabled={!hasSelectedAlternative}>
            Toma
          </Button>
        </AlternativesForm>
      </Widget.Content>
    </Widget>
  )
}

export default QuestionWidget
