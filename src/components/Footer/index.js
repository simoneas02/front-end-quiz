import React from 'react'
import styled from 'styled-components'

const FooterWrapper = styled.footer`
  background-color: #00000070;
  padding: 20px;
  display: flex;
  align-items: center;
  border-radius: 4px;
  img {
    width: 58px;
    margin-right: 23px;
  }
  a {
    color: white;
    text-decoration: none;
    transition: 0.3s;
    &:hover,
    &:focus {
      opacity: 0.5;
    }
    span {
      text-decoration: underline;
    }
  }
`

export default function Footer(props) {
  return (
    <FooterWrapper {...props}>
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/6/63/Code_Icon.PNG"
        alt="code"
      />
      <p>
        by{' '}
        <a href="https://github.com/simoneas02/">
          <span>Simone Amorim</span>
        </a>
      </p>
    </FooterWrapper>
  )
}
