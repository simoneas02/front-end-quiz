import Head from 'next/head'

export default () => (
  <Head>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap"
      rel="stylesheet"
    />

    {'<!-- Primary Meta Tags -->'}
    <title>Frontend Quiz</title>
    <meta name="title" content="Frontend Quiz" />
    <meta
      name="description"
      content="A Frontend Quiz to play wiht friends!
          A Frontend Quiz to play wiht friends!"
    />

    {'<!-- Open Graph / Facebook -->'}
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://front-end-quiz.vercel.app/" />
    <meta property="og:title" content="Frontend Quiz" />
    <meta
      property="og:description"
      content="A Frontend Quiz to play wiht friends!"
    />
    <meta
      property="og:image"
      content="https://upload.wikimedia.org/wikipedia/commons/6/63/Code_Icon.PNG"
    />

    {'<!-- Twitter -->'}
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:url" content="https://front-end-quiz.vercel.app/" />
    <meta property="twitter:title" content="Frontend Quiz" />
    <meta
      property="twitter:description"
      content="A Frontend Quiz to play wiht friends!"
    />
    <meta
      property="twitter:image"
      content="https://upload.wikimedia.org/wikipedia/commons/6/63/Code_Icon.PNG"
    />
  </Head>
)
