import React from 'react'

import Widget from '../../components/Widget'

const LoadingWidget = () => (
  <Widget>
    <Widget.Header>Carregando...</Widget.Header>

    <Widget.Content>...</Widget.Content>
  </Widget>
)

export default LoadingWidget
